<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CounterSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Мои счётчики';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="counter-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Новый счётчик', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            [
                'class' => 'yii\grid\SerialColumn',
                'header' => '№'
            ],
            //'id',
            //'id_user',
            'name',
            [
                'attribute' => 'count',
                'format' => 'html',
                'contentOptions' => ['class' => 'align-center'],
                'headerOptions' => ['class' => 'align-center'],
                'value' => function ($data) {
                    return Html::a('-','/counter/decrement?id=' . $data->id, ['class' => 'change-counter minus', 'title' => 'Убавить'])
                        . '&nbsp;&nbsp;&nbsp;'
                        . $data -> count . '&nbsp;&nbsp;&nbsp;'
                        . Html::a('+','/counter/increment?id=' . $data->id, ['class' => 'change-counter plus', 'title' => 'Прибавить']);
                },
            ],
            [
                'format' => 'raw',
                'contentOptions' => ['class' => 'align-center'],
                'headerOptions' => ['class' => 'align-center'],
                'value' => function($data){
                    return Html::a(
                        Html::tag(
                            'span',
                            '',
                            [
                                'class' => 'glyphicon glyphicon-pencil',
                                'title' => 'Редактировать'
                            ]
                        ) . '&nbsp;&nbsp;Редактировать',
                        '/counter/update?id=' . $data->id,
                        ['class' => 'counter-edit']
                    ) . '<br>' . Html::a(
                            Html::tag(
                                'span',
                                '',
                                ['class' => 'glyphicon glyphicon-remove']
                            ) . '&nbsp;&nbsp;Удалить',
                            '/counter/delete?id=' . $data->id,
                            [
                                'class' => 'text-danger',
                                'title' => 'Удалить',
                                'aria-label' => 'Удалить',
                                'data' => [
                                    'pjax' => '0',
                                    'confirm' => 'Вы уверены, что хотите удалить этот элемент?',
                                    'method' => 'post'
                                ],
                            ]
                        );
                }
            ],
        ],
    ]); ?>
</div>
