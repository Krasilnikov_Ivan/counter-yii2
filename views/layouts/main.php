<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use Yii;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => 'Counter',
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => [
            ['label' => 'Мои счётчики', 'url' => ['/counter'], 'visible' => !Yii::$app->user->isGuest],
            ['label' => 'Контакты', 'url' => ['/site/contact']],
            Yii::$app->user->isGuest ? (
                ['label' => 'Войти', 'url' => ['/user/security/login']]
            ) : (
                Html::tag(
                    'ul',
                    Html::tag(
                        'li',
                        Html::a(
                            Yii::$app->user->identity->username . Html::tag('span', '', ['class' => 'caret']),
                            '#',
                            [
                                'class' => 'dropdown-toggle',
                                'data-toggle' => 'dropdown',
                                'role' => 'button',
                                'aria-haspopup' => 'true',
                                'aria-expanded' => 'false'
                            ]
                        ) . Html::tag(
                            'ul',
                            Html::tag(
                                'li',
                                Html::a('Мой профиль', '/user/profile/show?id=' . Yii::$app->user->identity->getId(),[]),
                                []
                            ) . Html::tag(
                                'li',
                                Html::a('Настройки профиля', '/user/settings/profile',[]),
                                []
                            ). Html::tag(
                                'li',
                                Html::a('Социальные сети', '/user/settings/networks',[]),
                                []
                            ) . Html::tag(
                                'li',
                                Html::a('Выход', '/user/security/logout',[]),
                                []
                            ),
                            ['class' => ['dropdown-menu', 'inverse']]
                        ),
                        ['class' => 'dropdown']),
                    ['class' => ['nav', 'navbar-nav', 'navbar-right']]
                )
                /*['label' => 'Выйти (' . Yii::$app->user->identity->username . ')',
                    'url' => ['/user/security/logout'],
                    'linkOptions' => ['data-method' => 'post']
                ]*/
            ),
            ['label' => 'Регистрация', 'url' => ['/user/registration/register'], 'visible' => Yii::$app->user->isGuest],
        ],
    ]);
    NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer inverse">
    <div class="container">
        <p class="pull-left">&copy; Красильников Иван <?= date('Y') == '2017' ? '2017' : '2017 - ' . date('Y') ?></p>

        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
