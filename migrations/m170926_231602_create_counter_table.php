<?php

use yii\db\Migration;

/**
 * Handles the creation of table `counter`.
 */
class m170926_231602_create_counter_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('counter', [
            'id' => $this->primaryKey(),
            'id_user' => $this->integer(),
            'name' => $this->string()->notNull(),
            'count' => $this->integer()->notNull(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('counter');
    }
}
